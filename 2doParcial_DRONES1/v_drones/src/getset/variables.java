
package getset;

public class variables {
    private static String idtecnico;
    private static String cedula;
    private static String nombre;
    private static String apellido;
    private static String sexo;
    private static String telefono;
    private static String cmant;

    public String getIdtecnico() {
        return idtecnico;
    }

    public void setIdtecnico(String idtecnico) {
        this.idtecnico = idtecnico;
    }

    public String getCedula() {
        return cedula;
    }

    public void setCedula(String cedula) {
        this.cedula = cedula;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public String getSexo() {
        return sexo;
    }

    public void setSexo(String sexo) {
        this.sexo = sexo;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getCmant() {
        return cmant;
    }

    public void setCmant(String cmant) {
        this.cmant = cmant;
    }
}
