
package getset;


public class variables2 {
    private static String idencargado;
    private static String cedula;
    private static String nombre;
    private static String apellido;
    private static String telefono;
    private static String ijornada;
    private static String fjornada;
    private static String ngraba;

    public static String getIdencargado() {
        return idencargado;
    }

    public static void setIdencargado(String idencargado) {
        variables2.idencargado = idencargado;
    }

    public static String getCedula() {
        return cedula;
    }

    public static void setCedula(String cedula) {
        variables2.cedula = cedula;
    }

    public static String getNombre() {
        return nombre;
    }

    public static void setNombre(String nombre) {
        variables2.nombre = nombre;
    }

    public static String getApellido() {
        return apellido;
    }

    public static void setApellido(String apellido) {
        variables2.apellido = apellido;
    }

    public static String getTelefono() {
        return telefono;
    }

    public static void setTelefono(String telefono) {
        variables2.telefono = telefono;
    }

    public static String getIjornada() {
        return ijornada;
    }

    public static void setIjornada(String ijornada) {
        variables2.ijornada = ijornada;
    }

    public static String getFjornada() {
        return fjornada;
    }

    public static void setFjornada(String fjornada) {
        variables2.fjornada = fjornada;
    }

    public static String getNgraba() {
        return ngraba;
    }

    public static void setNgraba(String ngraba) {
        variables2.ngraba = ngraba;
    }

    
}
