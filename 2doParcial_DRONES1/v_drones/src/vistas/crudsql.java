package vistas;
import getset.variables;
import java.sql.Connection;
import javax.swing.JOptionPane;
import java.sql.ResultSet;
import sql.conexionsql;
//CODIGO FUNCIONAL PARA INSERTAR DATOS DE TECNICOS
class crudsql extends conexionsql{
   
    java.sql.Statement st;
    ResultSet rs;
    variables var=new variables();
    public void insertar(String id, String ced, String nom, String apell, String sexo, String tlf, String cmant){
        try {
            Connection conexion=conectar();
            st=conexion.createStatement(); //Ejecutar sentencias sql
            String sql="insert into tecnico(id_tecnico,ced_tecnico,nom_tecnico,apell_tecnico,sexo_tecnico,tlf_tecnico,cantmant_tecnico) values('"+id+"','"+ced+"','"+nom+"','"+apell+"','"+sexo+"','"+tlf+"','"+cmant+"')";
            st.execute(sql);
            st.close();
            conexion.close();
            JOptionPane.showMessageDialog(null,"El tecnico se guardo correctamente", "Mensaje", JOptionPane.INFORMATION_MESSAGE);
        } catch (Exception e){
            JOptionPane.showMessageDialog(null,"El tecnico no se guardo"+e, "Mensaje", JOptionPane.ERROR_MESSAGE);
        }
    }
    public void mostrar(String id_tecnico){
        try{
            Connection conexion=conectar();
            st=conexion.createStatement();
            String sql="Select * from tecnico where id_tecnico='"+id_tecnico+"';";
            rs=st.executeQuery(sql);
            if(rs.next()){
                var.setIdtecnico(rs.getString("id_tecnico"));
                var.setCedula(rs.getString("ced_tecnico"));
                var.setNombre(rs.getString("nom_tecnico"));
                var.setApellido(rs.getString("apell_tecnico"));
                var.setSexo(rs.getString("sexo_tecnico"));
                var.setTelefono(rs.getString("tlf_tecnico"));
                var.setCmant(rs.getString("cantmant_tecnico"));
            }else{
                var.setIdtecnico("");
                var.setCedula("");
                var.setNombre("");
                var.setApellido("");
                var.setSexo("");
                var.setTelefono("");
                var.setCmant("");
                JOptionPane.showMessageDialog(null, "No se encontro registro","Sin registro", JOptionPane.INFORMATION_MESSAGE);
            }
            st.close();
            conexion.close();
        }catch (Exception e){
            JOptionPane.showMessageDialog(null, "Error en la busqueda","Error busqueda", JOptionPane.ERROR_MESSAGE);
        }
    }
    public void actualizar(String ced, String nom, String apell, String sexo, String tlf, String cmant, String id){
        try{
            Connection conexion=conectar();
            st=conexion.createStatement();
            String sql="update tecnico set ced_tecnico='"+ced+"', nom_tecnico='"+nom+"', apell_tecnico='"+apell+"', sexo_tecnico='"+sexo+"', tlf_tecnico='"+tlf+"', cantmant_tecnico='"+cmant+"' where id_tecnico='"+id+"'; ";
            st.executeUpdate(sql);
            st.close();
            conexion.close();
            JOptionPane.showMessageDialog(null, "El tecnico fue actualizado correctamente", "Exito", JOptionPane.INFORMATION_MESSAGE);
        } catch (Exception e){
            JOptionPane.showMessageDialog(null, "Error al actualizar "+e,"Error", JOptionPane.ERROR_MESSAGE);
            
        }
    }
    
    public void eliminar(String id){
        try{
            Connection conexion=conectar();
            st=conexion.createStatement();
            String sql="delete from tecnico where id_tecnico='"+id+"'; ";
            st.executeUpdate(sql);
            st.close();
            conexion.close();
            JOptionPane.showMessageDialog(null, "Técnico eliminado correctamente", "Eliminar", JOptionPane.INFORMATION_MESSAGE);
        } catch (Exception e){
            JOptionPane.showMessageDialog(null, "Error al eliminar técnico"+ e, "Error", JOptionPane.ERROR_MESSAGE);
        }
    }
    
   
}
