
package vistas;
import getset.variables2;
import java.sql.Connection;
import javax.swing.JOptionPane;
import java.sql.ResultSet;
import sql.conexionsql;

public class crudsql2 extends conexionsql{
    java.sql.Statement st;
    ResultSet rs;
    variables2 var=new variables2();
    public void insertar(String id, String ced, String nom, String apell, String tlf, String ijornada, String fjornada, String ngraba){
        try {
            Connection conexion=conectar();
            st=conexion.createStatement(); //Ejecutar sentencias sql
            String sql="insert into encargado(id_encargado,ced_encargado,nombre_encargado,apell_encargado,tlf_encargado,inicio_jornada,fin_jornada,num_grabaciones) values('"+id+"','"+ced+"','"+nom+"','"+apell+"','"+tlf+"','"+ijornada+"','"+fjornada+"','"+ngraba+"')";
            st.execute(sql);
            st.close();
            conexion.close();
            JOptionPane.showMessageDialog(null,"El encargado se guardo correctamente", "Mensaje", JOptionPane.INFORMATION_MESSAGE);
        } catch (Exception e){
            JOptionPane.showMessageDialog(null,"El encargado no se guardo"+e, "Mensaje", JOptionPane.ERROR_MESSAGE);
        }
    }
    public void mostrar(String id_encargado){
        try{
            Connection conexion=conectar();
            st=conexion.createStatement();
            String sql="Select * from encargado where id_encargado='"+id_encargado+"';";
            rs=st.executeQuery(sql);
            if(rs.next()){
                var.setIdencargado(rs.getString("id_encargado"));
                var.setCedula(rs.getString("ced_encargado"));
                var.setNombre(rs.getString("nombre_encargado"));
                var.setApellido(rs.getString("apell_encargado"));
                var.setTelefono(rs.getString("tlf_encargado"));
                var.setIjornada(rs.getString("inicio_jornada"));
                var.setFjornada(rs.getString("fin_jornada"));
                var.setNgraba(rs.getString("num_grabaciones"));
            }else{
                var.setIdencargado("");
                var.setCedula("");
                var.setNombre("");
                var.setApellido("");
                var.setTelefono("");
                var.setIjornada("");
                var.setFjornada("");
                var.setNgraba("");
                JOptionPane.showMessageDialog(null, "No se encontro registro","Sin registro", JOptionPane.INFORMATION_MESSAGE);
            }
            st.close();
            conexion.close();
        }catch (Exception e){
            JOptionPane.showMessageDialog(null, "Error en la busqueda","Error busqueda", JOptionPane.ERROR_MESSAGE);
        }
    }
    public void actualizar(String ced, String nom, String apell, String tlf, String ijornada, String fjornada, String ngraba, String id){
        try{
            Connection conexion=conectar();
            st=conexion.createStatement();
            String sql="update encargado set ced_encargado='"+ced+"', nombre_encargado='"+nom+"', apell_encargado='"+apell+"', tlf_encargado='"+tlf+"', inicio_jornada='"+ijornada+"', fin_jornada='"+fjornada+"', num_grabaciones='"+ngraba+"' where id_encargado='"+id+"'; ";
            st.executeUpdate(sql);
            st.close();
            conexion.close();
            JOptionPane.showMessageDialog(null, "El encargado fue actualizado correctamente", "Exito", JOptionPane.INFORMATION_MESSAGE);
        } catch (Exception e){
            JOptionPane.showMessageDialog(null, "Error al actualizar "+e,"Error", JOptionPane.ERROR_MESSAGE);
            
        }
    }
    public void eliminar(String id){
        try{
            Connection conexion=conectar();
            st=conexion.createStatement();
            String sql="delete from encargado where id_encargado='"+id+"'; ";
            st.executeUpdate(sql);
            st.close();
            conexion.close();
            JOptionPane.showMessageDialog(null, "Encargado eliminado correctamente", "Eliminar", JOptionPane.INFORMATION_MESSAGE);
        } catch (Exception e){
            JOptionPane.showMessageDialog(null, "Error al eliminar encargado"+ e, "Error", JOptionPane.ERROR_MESSAGE);
        }
    }
}
