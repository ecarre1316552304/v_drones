
/*
|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
-----------------PROCEDIMIENTO: TRANSACCION DRON (INGRESO)----------------
|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
*/
CREATE OR REPLACE PROCEDURE public.ingreso_dron (integer, varchar, integer, date)
LANGUAGE 'plpgsql' AS $BODY$
DECLARE 
	 N1 int;
	BEGIN
		INSERT INTO DRON (SERIAL_DRON, FABRICANTE_DRON, HORAS_DRON, ADQUISICION_DRON, GARANTIA) VALUES ($1, $2, $3, $4, 'true');
				
		IF ($3 > 3000 ) THEN 
			RAISE EXCEPTION 'NO ESTA PERMITIDO INGRESAR MAS DE 3000 HORAS OPERATIVAS';
		ELSE 
			UPDATE DRON SET HORAS_DRON=$3 WHERE SERIAL_DRON = $1;
		END IF;		
		
	EXCEPTION
		WHEN SQLSTATE '23514' THEN
		RAISE EXCEPTION 'ERROR AL INGRESAR NUEVAS HORAS DE DRON';
		ROLLBACK;
	COMMIT;
END;
$BODY$;

--CALL ingreso_dron ('32266', '1500', '2021-05-10');

/*
|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
-----------------PROCEDIMIENTO: TRANSACCION DRON (ASIGNACION)----------------
|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
*/

CREATE OR REPLACE PROCEDURE public.asignar_tecnico(integer, integer, date, integer)
LANGUAGE 'plpgsql' AS $BODY$
DECLARE 
	 N1 int;
	BEGIN
		INSERT INTO MANTENIMIENTO (ID_MANTENIMIENTO, ID_TECNICO, FECHA_MANTENIMIENTO , CANT_MANTENIMIENTO) VALUES ($1, $2, $3, $4);
		SELECT CANTMANT_TECNICO INTO N1 FROM TECNICO WHERE TECNICO.ID_TECNICO = $2;
		
		IF (N1 > 40) THEN
			RAISE EXCEPTION 'EL TECNICO HA EXCEDIDO LA CANTIDAD DE MANTENIMIENTOS';
		END IF;
	EXCEPTION
		WHEN SQLSTATE '23514' THEN
		RAISE EXCEPTION 'ERROR AL ASIGNAR EL DRON';
		ROLLBACK;
	COMMIT;
END;
$BODY$;

--CALL asignar_tecnico ('235', '128', '2020/12/15', '41');


